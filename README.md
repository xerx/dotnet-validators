# dotnet-xrx-validators

### Simple fluent object validator
[![Nuget](https://img.shields.io/nuget/v/Xrx.Validators.svg?style=for-the-badge)](https://www.nuget.org/packages/Xrx.Validators/)

#### Create your validator for an object
```c#
var validator = new Validator<Traveler>();
```

#### Perform simple validations
```c#
var result = validator.Item(traveler)
                      .Should(p => p.Age > 16)
                      .Result;

```

#### Or more complex ones
```c#
var result = validator.Item(traveler)
                      .Should(t => t.Age > 16)
                      .Or(t => t.IsAccompanied)
                      .AndShould(t => t.HasPassport)
                      .Or(t => t.Country == "Greece")
                      .Result;
//Equals
var bool = (traveler.Age > 16 || traveler.IsAccompanied) && (traveler.HasPassport || traveler.Country == "Greece");
```

#### Catch exceptions
```c#
var result = validator.Item(nullObject)
                      .Should(n => n.SomeProperty > 16)
                      .Result;
if(result.Exception != null)
{
    Console.WriteLine(result.Exception.Message);
}
```

#### Use extensions
```c#
var validator = new Validator<User>();
var result = validator.Item(user)
                      .Should(u => u.Email.IsEmail())
                      .And(u => u.Password.ContainsLetterNumberSymbol())
                      .Result;

```