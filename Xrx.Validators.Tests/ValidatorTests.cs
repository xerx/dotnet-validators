﻿using System;
using Shouldly;
using Xunit;
using Xrx.Validators.Extensions;

namespace Xrx.Validators.Tests
{

    public class ValidatorsTests
    {
        private readonly ValidationObject vObject;
        public ValidatorsTests()
        {
            vObject = new ValidationObject
            {
                Text = "Some Text",
                Number = 10
            };
        }

        [Fact]
        private void Validator_SimpleConditionShouldWork()
        {
            var validator = new Validator<ValidationObject>();
            var result = validator.Item(vObject)
                                  .Should(x => x.Text != null)
                                  .Result;
            result.IsValid.ShouldBe(true);
        }

        [Fact]
        private void Validator_ShouldReturnExceptionAndIsInvalid()
        {
            var @object = new ValidationObject();
            var validator = new Validator<ValidationObject>();
            var result = validator.Item(@object)
                                  .Should(x => x != null)
                                  .Or(x => x.Text.Length > 0)
                                  .Result;
            result.ShouldNotBeNull();
            result.IsValid.ShouldBe(false);
            result.Exception.ShouldNotBeNull();
        }
    }
}
