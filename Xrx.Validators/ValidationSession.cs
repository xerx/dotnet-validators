﻿using System;
namespace Xrx.Validators
{
    public class ValidationSession 
    {
        public Func<bool, bool, bool> conditionalOperation;
        public bool IsValid { get; set; }
    }
}
