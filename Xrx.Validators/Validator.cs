﻿using System;
using System.Collections.Generic;
using Xrx.Validators.Abstractions;
using Xrx.Validators.Helpers;

namespace Xrx.Validators
{
    public class Validator<T> : IValidator<T>, IDisposable
    {
        protected List<ValidationSession> sessions;
        protected ValidationSession currentSession;
        protected T item;

        private bool OperationBroke;

        private ValidationResult result;
        public ValidationResult Result
        {
            set => result = value;
            get
            {
                if(result == null)
                {
                    result = ValidatorHelper.EvaluateResult(sessions);
                }
                return result;
            }
        }

        public IItemConsumer<T> Item(T item)
        {
            this.item = item;
            sessions = new List<ValidationSession>();
            return this;
        }

        public Abstractions.IComparer<T> Should(Func<T, bool> predicate)
        {
            if (OperationBroke) { return this; }
            currentSession = new ValidationSession();
            sessions.Add(currentSession);
            currentSession.IsValid = InvokePredicate(predicate);
            return this;
        }

        public Abstractions.IComparer<T> AndShould(Func<T, bool> predicate)
        {
            return StartNewSession(predicate, ValidatorHelper.AndOperation);
        }
        public Abstractions.IComparer<T> OrShould(Func<T, bool> predicate)
        {
            return StartNewSession(predicate, ValidatorHelper.OrOperation);
        }
        private Abstractions.IComparer<T> StartNewSession(Func<T, bool> predicate, Func<bool, bool, bool> operation)
        {
            if (OperationBroke) { return this; }
            ValidationSession session = new ValidationSession
            {
                conditionalOperation = operation
            };
            sessions.Add(session);
            currentSession = session;
            currentSession.IsValid = InvokePredicate(predicate);
            return this;
        }
        public ISessionComparer<T> And(Func<T, bool> predicate)
        {
            if (OperationBroke) { return this; }
            currentSession.IsValid = InvokePredicate(predicate) && currentSession.IsValid;
            return this;
        }

        public ISessionComparer<T> Or(Func<T, bool> predicate)
        {
            if (OperationBroke) { return this; }
            currentSession.IsValid = InvokePredicate(predicate) || currentSession.IsValid;
            return this;
        }
        private bool InvokePredicate(Func<T, bool> predicate)
        {
            try
            {
                var r = predicate.Invoke(item);
                return r;
            }
            catch (Exception ex)
            {
                OperationBroke = true;
                Result = new ValidationResult { Exception = ex };
            }
            return false;
        }
        public virtual void Clear()
        {
            sessions.Clear();
            currentSession = null;
            result = null;
        }
        public virtual void Dispose()
        {
            sessions = null;
            currentSession = null;
            result = null;
        }
    }
}
