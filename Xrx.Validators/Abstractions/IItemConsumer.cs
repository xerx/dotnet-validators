﻿using System;

namespace Xrx.Validators.Abstractions
{
    public interface IItemConsumer<T>
    {
        IComparer<T> Should(Func<T, bool> predicate);
    }
}
