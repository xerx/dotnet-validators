﻿using System;
namespace Xrx.Validators.Abstractions
{
    public interface ISessionComparer<T> : IComparer<T>
    {
        IComparer<T> AndShould(Func<T, bool> predicate);
        IComparer<T> OrShould(Func<T, bool> predicate);
    }
}
