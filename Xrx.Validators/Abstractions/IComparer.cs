﻿using System;
namespace Xrx.Validators.Abstractions
{
    public interface IComparer<T>
    {
        ISessionComparer<T> And(Func<T, bool> predicate);
        ISessionComparer<T> Or(Func<T, bool> predicate);
        ValidationResult Result { get; }
    }
}
