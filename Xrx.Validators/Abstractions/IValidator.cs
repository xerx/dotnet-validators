﻿using System;
namespace Xrx.Validators.Abstractions
{
    public interface IValidator<T> : IItemConsumer<T>, ISessionComparer<T>
    {
        IItemConsumer<T> Item(T item);
    }
}
