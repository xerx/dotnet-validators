﻿using System;

namespace Xrx.Validators
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
    }
}
