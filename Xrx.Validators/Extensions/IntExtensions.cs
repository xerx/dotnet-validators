﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xrx.Validators.Extensions
{
    public static class IntExtensions
    {
        public static bool IsEven(this int self)
        {
            return self % 2 == 0;
        }
    }
}
