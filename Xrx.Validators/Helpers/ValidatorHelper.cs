﻿using System;
using System.Collections.Generic;

namespace Xrx.Validators.Helpers
{
    public static class ValidatorHelper
    {
        public static ValidationResult EvaluateResult(List<ValidationSession> sessions)
        {
            ValidationResult finalResult = new ValidationResult
            {
                IsValid = sessions[0].IsValid
            };
            sessions.RemoveAt(0);
            for (int i = 0; i < sessions.Count; i++)
            {
                finalResult.IsValid = sessions[i]
                                        .conditionalOperation(finalResult.IsValid, sessions[i].IsValid);
            }
            return finalResult;
        }
        public static bool OrOperation(bool factor1, bool factor2)
        {
            return factor1 || factor2;
        }
        public static bool AndOperation(bool factor1, bool factor2)
        {
            return factor1 && factor2;
        }
    }
}
